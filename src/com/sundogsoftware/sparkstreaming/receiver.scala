package com.sundogsoftware.sparkstreaming

import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.receiver.Receiver
import scala.util.parsing.json.JSON
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import java.util.regex.Pattern
import java.util.regex.Matcher
import scala.xml._
import java.text.SimpleDateFormat
import play.api.libs.json._
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity


/**
 * Spark Streaming Example TfL Receiver
 */

object receiver {

  private val tflUrl = "https://tfl.gov.uk/tfl/syndication/feeds/tims_feed.xml?app_id=09a40888&app_key=b8e2f385b58c9be640ff95c0c3c930f7"


  class TFLTimsFeed() extends Receiver[String](StorageLevel.MEMORY_ONLY) with Runnable {

    @transient
    private var thread: Thread = _

    override def onStart(): Unit = {
      thread = new Thread(this)
      thread.start()
    }

    override def onStop(): Unit = {
      thread.interrupt()
    }

    override def run(): Unit = {
      while (true) {
        receive();
        Thread.sleep(150 * 1000); // 2 minutos y medio
      }
    }

    private def receive(): Unit = {

      val inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
      val outputFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm")

      val httpClient = new DefaultHttpClient();
      val getRequest = new HttpGet(tflUrl);
      getRequest.addHeader("accept", "application/json");

      val response = httpClient.execute(getRequest);
      if (response.getStatusLine().getStatusCode() != 200) {
        throw new RuntimeException("Failed : HTTP error code : "
          + response.getStatusLine().getStatusCode());
      }

      val br = new BufferedReader(
        new InputStreamReader((response.getEntity().getContent())));

      val str = Stream.continually(br.readLine()).takeWhile(_ != null).mkString("\n")

      val xml = scala.xml.XML.loadString(str)
      
      //val locations = xml.\\("Disruption").\("location").text
      //val locationsReplace = locations.replace("[", "-->[")
      //var seq: Seq[String] = locationsReplace.split("-->").toSeq.map(_.trim).filter(_ != "")

      var arrjson: JsArray = Json.arr()
      
      for {
        disruption <- xml \\ "Disruption"
        location <- disruption \ "location"
        status <- disruption \ "status"
        severity <- disruption \ "severity"
        category <- disruption \ "category"
        startTime <- disruption \ "startTime"
        endTime <- disruption \ "endTime"
        comments <- disruption \ "comments"
        currentUpdate <- disruption \ "currentUpdate"
        causearea <- disruption \ "CauseArea"
        displaypoint <- causearea \ "DisplayPoint"
        point <- displaypoint \ "Point"
        coordinates <- point \ "coordinatesLL"
      } yield {
        if (status.text == "Active" || status.text == "Active Long Term") {

          val splitcord = coordinates.text.split(",")
          val longitud = "-0" + splitcord(0).substring(1)
          val latitud = splitcord(1)
          val coordenadas = "(" + latitud + "," + longitud + ")"

          val objeto: JsValue = Json.obj("location" -> location.text, "status" -> status.text, "severity" -> severity.text,
            "category" -> category.text, "startingtime" -> startTime.text,
            "endtime" -> endTime.text, "comments" -> comments.text,
            "currentupdate" -> currentUpdate.text, "lat" -> latitud, "lon" -> longitud)

          arrjson = arrjson :+ (objeto)
          
          println("Incidencia ID: " + disruption \@ "id")
          println("Localización: " + location.text)
          println("Estado: " + status.text)
          println("Importancia: " + severity.text)
          println("Categoría: " + category.text)
          println("T.inicio: " + outputFormat.format(inputFormat.parse(startTime.text)))
          println("T.fin: " + outputFormat.format(inputFormat.parse(endTime.text)))
          println("Comentarios: " + comments.text)
          println("Estado actual: " + currentUpdate.text)
          println("Estado: " + status.text)
          println("Coordenadas: " + coordenadas)
          println("Latitud: " + latitud + " - Longitud: " + longitud)
          println("=====================================================================")
        }
      }

    
      val readableString: String = Json.prettyPrint(arrjson)
      println(readableString)
      val post = new HttpPost("http://localhost:8080/")
      post.setHeader("Content-type", "application/json")
      post.setEntity(new StringEntity(readableString))
      val responses = (new DefaultHttpClient).execute(post)
      println("--- HEADERS ---")
      response.getAllHeaders.foreach(arg => println(arg))

    }
  }

}