package com.sundogsoftware.sparkstreaming

import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{StructType, StructField, StringType, DoubleType}
//import com.databricks.spark.xml._
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}
import receiver._
import scala.xml._
import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.spark.streaming._
import org.apache.spark.streaming.twitter._
import org.apache.spark.streaming.StreamingContext._
import org.apache.log4j.Level
import Utilities._
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.DefaultHttpClient;
import edu.stanford.nlp.sentiment._

import org.apache.spark.{SparkConf, rdd}
import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}
import play.api.libs.json._


object exampleXML {
  

  
  def main(args: Array[String]) {
      
      val keywords: Seq[String] = Seq("tfl", "TfLTrafficNews","@TfLTrafficNews","disruption","traffic", "road is closed","accident", "incident","collision","roadworks","breakdown","update","cleared","traffic light","emergency repairs")
      setupTwitter()
      setupLogging()

      val ssc = new StreamingContext("local[*]", "exampleXML", Seconds(5))
      val tweets = TwitterUtils.createStream(ssc, None, keywords)
      
      //val statuses = tweets.map(status => status.getText())
      
      var totalTweets:Long = 0
      var contadortweets:Long = 0      
        
      tweets.foreachRDD((rdd, time) => {
      // Don't bother with empty batches
      if (rdd.count() > 0) {
        // Combine each partition's results into a single RDD:
        val repartitionedRDD = rdd.repartition(1).cache()
        // And print out a directory with the results.
        //println("Tweets_" + time.milliseconds.toString)
        // Stop once we've collected 1000 tweets.
        val tweets = repartitionedRDD.collect()
        for(tweet <- tweets){
          contadortweets += 1
          val sentiment = SentimentAnalysisUtils.detectSentiment(tweet.getText).toString
          //val place = tweet.getPlace
          val location = tweet.getUser.getLocation
          val userName = tweet.getUser.getName
          val userAcc =tweet.getUser.getScreenName
          val imgurl = tweet.getUser.getProfileImageURL
          val timestamp = tweet.getCreatedAt.toString()
          //val coordinates = "("+tweet.getGeoLocation.getLatitude+","+tweet.getGeoLocation.getLongitude+")"
          if(location !=null){
            if(location.contains("London") && (sentiment == "NEUTRAL" || sentiment == "POSITIVE" || sentiment == "VERY_POSITIVE")){
              println("Sentimiento: "+sentiment + " Tweet #"+contadortweets+ ": " + tweet.getText + " Localización: "+location)
              
              val objeto: JsValue = Json.obj("number" -> contadortweets, "text" -> tweet.getText, "userName" -> userName,
            "userAcc" -> userAcc, "imgUrl" -> imgurl,
            "location" -> location, "timestamp" -> timestamp)
            
            val readableString: String = Json.prettyPrint(objeto)
            println(readableString)
              
              val post = new HttpPost("http://localhost:8080/tweets")
              post.setHeader("Content-type", "application/json")
              post.setEntity(new StringEntity(readableString))
              val responses = (new DefaultHttpClient).execute(post)
              println("--- HEADERS TWEET POST ---")
              responses.getAllHeaders.foreach(arg => println(arg))
            }
          }
        }
        totalTweets += repartitionedRDD.count()
        println("Tweet count: " + totalTweets)
        //if (totalTweets > 1000) {
        //  System.exit(0)
        //}
      }
    })
      
 
      //statuses.print()
      val stream = ssc.receiverStream(new TFLTimsFeed())
      stream.print()
   
      if (args.length > 2) {
         stream.saveAsTextFiles(args(2))
      }

      ssc.start()
      ssc.awaitTermination()

    }  
}